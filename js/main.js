class Jeu {
  initialiserJeu() {
    document.getElementById('canvas').style.backgroundColor = '#33cccc';
    const canvas = document.getElementById('canvas');

    if (canvas.getContext) {
      self = this;
      canvas.height = 750;
      canvas.width = 1500;
      const cadenas = new Cadenas(canvas);
      Cadenas.drawLock();
      Cadenas.drawCircle();
      Cadenas.placeObstacle();
      Cadenas.rotate();
      document.addEventListener('keypress', function(event) {
        if (event.code === 'Space') {
          if (Cadenas.validatePointer('Space') && Cadenas.actualLevel === 1) {
            Cadenas.victory();
          } else if (Cadenas.validatePointer('Space')) {
            Cadenas.counter = !Cadenas.counter;
            Cadenas.placeObstacle();
            Cadenas.actualLevel--;
          } else {
            Cadenas.onLoss();
          }
        } else if (event.code === 'Enter' && (cadenas.rotations.length === 0 ||
          Cadenas.isVictory || Cadenas.isDefeat)) {
          cadenas.play();
        } else if (event.code === 'Enter' && Cadenas.isStart) {
          Cadenas.isStart = false;
          cadenas.play();
        }
      });
    }
  }
}

class Cadenas {
  rotations = [];
  turnSpeed = 15;

  static level;
  static actualLevel;
  static counter = false;
  static isStart = true;
  static isVictory = false;
  static isDefeat = false;
  static xObstacle;
  static yObstacle;
  static angleObstacle;
  static angle = 3 * Math.PI / 2;
  static isOnPoint = false;
  width;
  height;

  constructor(p_canvas) {
    const canvas = p_canvas;
    Cadenas.level = 1;
    Cadenas.actualLevel = Cadenas.level;
    self.height = canvas.height / 2;
    self.width = canvas.width / 2;
    self.ctx = canvas.getContext('2d');
  }

  play() {
    if (Cadenas.isVictory || Cadenas.isDefeat) {
      if (Cadenas.isVictory) {
        Cadenas.level++;
        Cadenas.actualLevel = Cadenas.level;
      }
      Cadenas.reset();
      this.rotations.forEach(clearInterval);
    } else {
      this.rotations.push(setInterval(Cadenas.rotate,
          this.turnSpeed, 'rotate'));
    }
  }

  static reset() {
    Cadenas.isStart = true;
    Cadenas.isVictory = false;
    Cadenas.isDefeat = false;
    Cadenas.counter = false;
    Cadenas.isOnPoint = false;
    Cadenas.actualLevel = Cadenas.level;
    document.getElementById('canvas').style.backgroundColor = ' #33cccc';

    document.body.style.backgroundColor = '#33cccc';
    Cadenas.angle = 3 * Math.PI / 2;
    self.ctx.resetTransform();
    self.ctx.clearRect(0, 0, self.width * 2, self.height * 2);
    Cadenas.drawLock();
    Cadenas.drawCircle();
    Cadenas.placeObstacle();
    Cadenas.rotate();
  }

  static drawLock() {
    const ctx = self.ctx;
    const backgroundColor = document.getElementById('canvas')
        .style.backgroundColor;
    ctx.beginPath();
    ctx.fillStyle = 'dimgrey';
    ctx.arc(self.width, self.height - 175,
        (self.width + self.height) / 12, Math.PI, 2 * Math.PI);
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = backgroundColor;
    ctx.arc(self.width, self.height - 175,
        (self.width + self.height) / 22, 0, 2 * Math.PI);
    ctx.fill();

    ctx.fillStyle = 'dimgrey';
    ctx.beginPath();
    ctx.fillRect(self.width + 51, self.height - 175, 43, 100);

    ctx.beginPath();
    ctx.fillRect(self.width - 94, self.height - 175, 43, 100);
    ctx.closePath();
  }

  static drawCircle() {
    const backgroundColor = document.getElementById('canvas')
        .style.backgroundColor;
    const ctx = self.ctx;
    ctx.beginPath();
    ctx.arc(self.width, self.height,
        (self.width + self.height) / 8, 0, 2 * Math.PI);
    ctx.fillStyle = 'black';
    ctx.fill();

    ctx.beginPath();
    ctx.arc(self.width, self.height,
        (self.width + self.height) / 12.5, 0, 2 * Math.PI);
    ctx.fillStyle = backgroundColor;
    ctx.fill();
    Cadenas.drawNumber();
  }

  static drawNumber() {
    const level = Cadenas.actualLevel;
    const ctx = self.ctx;
    ctx.fillStyle = 'green';
    ctx.font = 'bold 80px Arial';
    if (level <= 9) {
      ctx.fillText(level.toString(), (self.width / 2) + 350,
          (self.height / 2) + 210);
    } else if (level > 10 && level < 100) {
      ctx.fillText(level.toString(), (self.width / 2) + 330,
          (self.height / 2) + 210);
    } else if (level >= 100) {
      ctx.fillText(level.toString(), (self.width / 2) + 310,
          (self.height / 2) + 210);
    }
    document.getElementById('level').innerText = 'Level: ' +
      Cadenas.level;
  }

  static drawBar() {
    const ctx = self.ctx;
    ctx.save();
    ctx.translate(((self.width + self.height) / 8) - 4, 10 / 2);
    ctx.rotate(-Math.PI / 2);
    ctx.beginPath();
    ctx.fillStyle = 'red';
    ctx.fillRect(0, 0, 10, -41);
    ctx.restore();
  }

  static rotate() {
    const ctx = self.ctx;
    if (Cadenas.isVictory || Cadenas.isDefeat) return;
    if (Cadenas.validatePointer('na')) {
      Cadenas.onLoss();
      return;
    }
    ctx.clearRect(self.width, self.height, 10, -140);
    ctx.save();
    Cadenas.drawCircle();
    Cadenas.drawObstacle();
    ctx.translate(self.width, self.height);
    ctx.rotate(Cadenas.angle);
    if (Cadenas.counter) {
      Cadenas.angle = (((Cadenas.angle + 2 * Math.PI) - Math.PI * 2 / 180) +
        2 * Math.PI) % (2 * Math.PI);
    } else {
      Cadenas.angle = (((Cadenas.angle + 2 * Math.PI) + Math.PI * 2 / 180) -
        2 * Math.PI) % (2 * Math.PI);
    }
    Cadenas.drawBar();
    ctx.translate(-self.width, -self.height);
    ctx.restore();
  }

  static validatePointer(keyCode) {
    let deltaA = Cadenas.angleObstacle - 0.08726646259 * 1.5;
    let deltaB = Cadenas.angleObstacle + 0.08726646259 * 1.5;
    let angle = Cadenas.angle;

    if (deltaA < 0) {
      deltaA += Math.PI;
      deltaB += Math.PI;
      angle = (angle + Math.PI) % (2 * Math.PI);
    } else if (deltaB > Math.PI * 2) {
      deltaA -= Math.PI;
      deltaB -= Math.PI;
      angle = ((angle + Math.PI * 2) - Math.PI) % (2 * Math.PI);
    }
    if (deltaA < angle && angle < deltaB) {
      Cadenas.isOnPoint = true;
    }

    if (keyCode === 'Space' &&
      (deltaA <= angle && angle <= deltaB)) {
      Cadenas.isOnPoint = false;
      return true;
    } else if (Cadenas.isOnPoint &&
      !Cadenas.counter && angle > deltaB) {
      console.log(Cadenas.isOnPoint);
      return true;
    } else if (Cadenas.isOnPoint &&
      Cadenas.counter && angle < deltaA) {
      console.log(Cadenas.isOnPoint);
      return true;
    }

    return false;
  }

  static victory() {
    const ctx = self.ctx;
    Cadenas.isVictory = true;
    ctx.resetTransform();
    ctx.clearRect(0, 0, self.width * 2,
        self.height * 2);
    Cadenas.drawCircle();
    ctx.translate(0, -100);
    Cadenas.drawLock();
  }

  static onLoss() {
    const ctx = self.ctx;
    let direction = 1;
    let cpt = 0;
    const level = Cadenas.level;
    Cadenas.isDefeat = true;
    document.getElementById('canvas')
        .style.backgroundColor = '#ff5c5c';
    document.body.style.backgroundColor = '#ff5c5c';
    ctx.clearRect(0, 0, self.width * 2,
        self.height * 2);
    Cadenas.drawLock();
    Cadenas.drawCircle();


    function vibrate() {
      ctx.clearRect(0, 0, self.width * 2,
          self.height * 2);
      ctx.resetTransform();
      if (direction === -1) {
        ctx.translate(-1, 0);
        Cadenas.drawLock();
        Cadenas.drawCircle();
        Cadenas.drawNumber(level);
      } else {
        ctx.translate(1, 0);
        Cadenas.drawLock();
        Cadenas.drawCircle();
        Cadenas.drawNumber(level);
      }
      direction = direction * -1;
      cpt++;
      if (cpt === 10) {
        return;
      }

      setTimeout(vibrate, 100);
    }

    setTimeout(vibrate, 100);
  }

  static placeObstacle() {
    const x = self.width;
    const y = self.height;
    const r = (((self.width + self.height) / 8) +
      ((self.width + self.height) / 12.5)) / 2;
    Cadenas.angleObstacle = Math.random() * Math.PI * 2;

    Cadenas.xObstacle = r * Math.cos(Cadenas.angleObstacle) + x;
    Cadenas.yObstacle = r * Math.sin(Cadenas.angleObstacle) + y;

    this.drawObstacle();
  }

  static drawObstacle() {
    const ctx = self.ctx;
    ctx.beginPath();
    ctx.arc(this.xObstacle, this.yObstacle, 10,
        0, Math.PI * 2);
    ctx.fillStyle = 'orange';
    ctx.fill();
  }
}

const jeu = new Jeu();

jeu.initialiserJeu();


